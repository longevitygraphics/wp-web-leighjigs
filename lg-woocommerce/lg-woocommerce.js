// Windows Ready Handler

(function ($) {
    $(document).ready(function () {
        $("#lg-product-tabs a").click(function(){
            if($(this).hasClass("active")){
                return true;
            }

            $("#lg-product-tabs a").removeClass("active");
            $(this).addClass("active");

            $("#lg-product-tabs-content .tab-pane").removeClass("show active");

            $($(this).attr("href")).addClass("show active");
        });


        /*$("#lg-product-tabs a").on('shown.bs.tab', function (e) {
            //debugger
            var currentTab = e.target;
            var tabLoaded = false;
            var tabSlug = $(currentTab).attr("data-slug");
            var productId = $(currentTab).attr("data-id");
            let url = $(currentTab).attr("href");
            var lgProductTabContent = $("lg-product-tabs-content");
            //check if tab is already loaded


            //get the tab content
            $.ajax({
                url: lg_ajax_obj.ajaxurl,
                data: {
                    'action': 'get_tab_content',
                    'tab': tabSlug,
                    'id': productId
                },
                success: function (data) {
                    console.log(data);
                    lgProductTabContent.append(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });

            window.history.pushState('object', '', url+ '/' + tabSlug);

            //stop browser from following the link
            e.preventDefault();
        });*/


    });

}(jQuery));
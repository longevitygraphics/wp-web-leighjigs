<?php
require 'layouts.php';

//pr(get_field("product_description_content"));

//pr(get_post_meta(22477));

/**
 * the product tabs
 */
$lg_product_tab_keys = array(
    array(
        'label' => 'Description',
        'name' => 'product_description_content',
        'key' => 'field_5d1be43675a05',
        'url_slug' => 'description'
    ),
    array(
        'label' => 'Features',
        'name' => 'product_features_content',
        'key' => 'field_5d1be89f51070',
        'url_slug' => 'features'
    ),
    array(
        'label' => 'How To',
        'name' => 'product_how_to_content',
        'key' => 'field_5d1be8a851071',
        'url_slug' => 'how-to'
    ),
    array(
        'label' => 'Videos',
        'name' => 'product_videos_content',
        'key' => 'field_5d1be8ac51072',
        'url_slug' => 'videos'
    ),
    array(
        'label' => 'Gallery',
        'name' => 'product_gallery_content',
        'key' => 'field_5d1be8b151073',
        'url_slug' => 'gallery'
    )
);

//this loop add the layouts to product tabs
foreach ($lg_product_tab_keys as $key => $value) {
    //add_filter( 'acf/load_field/key=' . $value['key'], 'acf_fc_load_fields' );
}


/**
 * remove all the woocommerce default tabs.
 **/
add_filter('woocommerce_product_tabs', 'woo_remove_product_tabs', 98);

function woo_remove_product_tabs($tabs)
{
    unset($tabs['description']);          // Remove the description tab
    unset($tabs['reviews']);          // Remove the reviews tab
    unset($tabs['additional_information']);   // Remove the additional information tab

    return $tabs;
}

/**
 * Add Content Below the Gallery Images @ WooCommerce Single Product
 */
add_action('woocommerce_after_single_product_summary',
    function () use ($lg_product_tab_keys) {
        lg_add_below_prod_gallery($lg_product_tab_keys);
    }, 5);

function lg_add_below_prod_gallery($args)
{
    lg_wc_flexible_layout($args);
}

add_action('init',
    function () use ($lg_product_tab_keys) {
        lg_add_rewrite_rules($lg_product_tab_keys);
    });
function lg_add_rewrite_rules($lg_product_tab_keys)
{
    /*add_rewrite_rule(
        '^product/([^/]*)/testing',
        'index.php?product=$matches[1]&tab=testing',
        'top'
    );*/
    foreach ($lg_product_tab_keys as $value) {
        //pr($value);
        add_rewrite_rule(
            '^product/([^/]*)/' . $value['url_slug'],
            'index.php?product=$matches[1]&tab=' . $value['url_slug'],
            'top'
        );
    }

}

add_filter('query_vars', 'wpse26388_query_vars');
function wpse26388_query_vars($query_vars)
{
    $query_vars[] = 'tab';
    $query_vars[] = 'id';

    return $query_vars;
}

add_action('wp_ajax_get_tab_content', 'get_tab_content');
add_action('wp_ajax_nopriv_get_tab_content', 'get_tab_content');
function get_tab_content()
{
    if (isset($_REQUEST)) {
        if (!empty($_REQUEST['tab']) and !empty($_REQUEST['id'])) {
            render_tab_pane($_REQUEST['tab'], $_REQUEST['id']);
        }

    }
}

add_action('wp_enqueue_scripts', 'lg_wp_enqueue_scripts');
function lg_wp_enqueue_scripts()
{
    wp_enqueue_script('lg-woocommerce', get_stylesheet_directory_uri() . '/lg-woocommerce/lg-woocommerce.js', array('jquery'), '1.0', true);

}
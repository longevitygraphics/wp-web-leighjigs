<?php

	/* Include theme functions */
	$function_dir_path = get_stylesheet_directory() . '/functions';

	$function_file_path = [
		'/basic/enqueue_styles_scripts.php',
		'/helper.php',
		'/advanced/actions-filters.php',
	];

	if($function_file_path && is_array($function_file_path)){
		foreach ($function_file_path as $key => $value) {
			if (file_exists($function_dir_path . $value)) {
			    require_once($function_dir_path . $value);
			}
		}
	}

	require_once get_stylesheet_directory() . '/components/main.php';

	/* end */

add_action( 'wp_enqueue_scripts', 'leigh_theme_enqueue_styles' );
function leigh_theme_enqueue_styles() {
    $parent_style = 'parent-style';
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
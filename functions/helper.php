<?php

# Help functions for the theme
function my_acf_flexible_content_layout_title( $title, $field, $layout, $i ) {
 
 // remove layout title from text
 $title = '';
 $new_title = get_sub_field('block_title');
 if($new_title){
   return $new_title;
 }else{
   return $title;
 }
 
}

// name
add_filter('acf/fields/flexible_content/layout_title', 'my_acf_flexible_content_layout_title', 10, 4);



?>